package procedures

import (
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/configs"
	"log"
	"os"

	client2 "github.com/CodeForPortland/ctl-aura/aura/client"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/handlers"

	"github.com/gammazero/nexus/client"
)

type Procedure struct {
	name string
	handler client.InvocationHandler
}

func InitializeProcedures() {

	logger := log.New(os.Stdout, "Procedures > ", log.LstdFlags)
	defer logger.Println("done initializing procedures")

	cfg := &configs.Configs{}
	cfg.Init()

	internalCLI := client2.AuraClient{
		Realm:  cfg.Realms.Internal.Realm,
		URL:    cfg.Realms.Internal.Url,
		Logger: logger,
	}

	logger.Println(internalCLI.URL)

	logger.Println(internalCLI.Realm)

	internalCLI.New(client2.ConnectWS)

	// EndUser
	internalCLI.RegisterService("store.endusers.create", handlers.CreateEndUser, nil)
	internalCLI.RegisterService("store.endusers.get", handlers.ReadEndUser, nil)
	internalCLI.RegisterService("store.endusers.update", handlers.UpdateEndUser, nil)
	internalCLI.RegisterService("store.endusers.check.email", handlers.IfEmail, nil)
	internalCLI.RegisterService("store.endusers.check.username", handlers.IfUsername, nil)

	// Questions
	internalCLI.RegisterService("store.questions.create", handlers.CreateQuestion, nil)
	internalCLI.RegisterService("store.questions.update", handlers.UpdateQuestion, nil)
	internalCLI.RegisterService("store.questions.get", handlers.GetQuestions, nil)
	internalCLI.RegisterService("store.questions.getall", handlers.GetAllQuestions, nil)

	//QuestionGroups
	internalCLI.RegisterService("store.questionsgroups.create", handlers.CreateQuestionsGroup, nil)
	internalCLI.RegisterService("store.questionsgroups.udpate", handlers.UpdateQuestionsGroup, nil)
	internalCLI.RegisterService("store.questionsgroups.getall", handlers.GetAllQuestionsGroups, nil)
	internalCLI.RegisterService("store.questionsgroups.getbyid", handlers.GetQuestionsGroupByID, nil)

	// Profiles
	internalCLI.RegisterService("store.profiles", handlers.ProfilesHandler, nil)
}