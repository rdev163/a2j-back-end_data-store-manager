package models

import (
	"errors"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/clients"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/utils"

	"github.com/google/uuid"

	"github.com/jinzhu/gorm"
)

type QuestionsGroup struct {
	gorm.Model
	QuestionsGroupID uuid.UUID
	Questions        []*Question `gorm:"many2many:questions"`
	Label            string `gorm:"index:idx_label"`
}

func (qg *QuestionsGroup) Create() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	if dbc.DB.NewRecord(qg) {
		dbc.DB.Create(&qg)
		if !dbc.DB.NewRecord(qg) {
			return nil
		}
	}

	return errors.New("failed to create new questionGroup record")
}

func (qg *QuestionsGroup) Update() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	questionGroup := QuestionsGroup{}

	dbc.DB.First(&questionGroup, "question_group_id = ?", qg.QuestionsGroupID).Update(qg)
	if utils.CompareModelValues(questionGroup, qg) {
		qg = &questionGroup
		return nil
	}

	return errors.New("failed to update questionGroup record")
}

func (qg *QuestionsGroup) FindByID(qgid uuid.UUID) error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	questionGroup := QuestionsGroup{}

	dbc.DB.First(&questionGroup, "question_group_id = ?", qgid)
	qg = &questionGroup
	if questionGroup.QuestionsGroupID == qgid {
		return nil
	}

	return errors.New("failed to find questionGroup by questionGroupID")
}

func (qg QuestionsGroup) GetAll() ([]*QuestionsGroup, error) {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	var questionGroups []*QuestionsGroup

	dbc.DB.Find(questionGroups)

	if len(questionGroups) > 0 {
		return questionGroups, nil
	}

	return questionGroups, errors.New("no QuestionGroups found in store")
}