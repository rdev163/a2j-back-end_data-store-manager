package models

import (
	"errors"
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/utils"
	"log"
	"os"
	"strings"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/clients"

	"github.com/google/uuid"

	"github.com/jinzhu/gorm"
)

var profileLogger = log.New(os.Stdout, "Profile > ", log.LstdFlags)

type ProfileType int

//noinspection GoSnakeCaseUsage
const (
	DEFAULT ProfileType = iota
	LAWYER
	LEGAL_ASSISTANT
	PETITIONER
	RESPONDENT
	ADMINISTRATOR
)

var ProfileTypes = [...]string{
	"DEFAULT",
	"LAWYER",
	"LEGAL_ASSISTANT",
	"PETITIONER",
	"RESPONDENT",
	"ADMINISTRATOR",
}

	/*==========================
		ProfileType Methods
	==========================*/

func (pt ProfileType) Parse(profileType string) ProfileType {
	test := strings.ToLower(profileType)
	switch test {
		case "lawyer":
			return LAWYER
		case "legalassistant", "legal_assistant":
			return LEGAL_ASSISTANT
		case "petitioner":
			return PETITIONER
		case "respondent":
			return RESPONDENT
		case "administrator":
			return ADMINISTRATOR
		default:
			return DEFAULT
	}
}

func (pt ProfileType) String() string {
	return ProfileTypes[pt]
}
	/*==========================
		</ ProfileType Methods
	==========================*/

type Profile struct {
	gorm.Model
	PID            uuid.UUID
	SessionHistory []string `gorm:"type:byte[]"`
	Type           ProfileType
	DataSets       interface{}
}

	/*==========================
		Profile Methods
	==========================*/

func (p *Profile) FindByID(pid uuid.UUID) error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	profile := Profile{}

	dbc.DB.First(&profile, "question_group_id = ?", pid)
	p = &profile
	if profile.PID == pid {
		return nil
	}

	return errors.New("failed to find questionGroup by questionGroupID")
}

func (p *Profile) Create() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer profileLogger.Println("Closing DB")

	if dbc.DB.NewRecord(p) {
		dbc.DB.Create(&p)
		if !dbc.DB.NewRecord(p) {
			return nil
		}
	}

	return errors.New("failed to create new questionGroup record")

}

func (p *Profile) Update() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	profile := Profile{}

	dbc.DB.First(&profile, "pid = ?", p.PID).Update(p)
	if utils.CompareModelValues(profile, p) {
		p = &profile
		return nil
	}

	return errors.New("failed to update profile record")
}

	/*==========================
		</ Profile Methods
	==========================*/
