package models

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/clients"
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/utils"

	"github.com/google/uuid"

	"github.com/jinzhu/gorm"
)

type QuestionType int

const (
	SELECT QuestionType = iota + 1
	TEXT
	NUMBER
	DATE
	BOOLEAN
	ADDRESS
	SECURE_TEXT
	SECURE_NUMBER
)

var questionTypeStrings = [...]string{
	"Select",
	"Text",
	"Number",
	"Date",
	"Boolean",
	"Address",
	"SecureText",
	"SecureNumber",
}

type Question struct {
	gorm.Model
	Label string
	QuestionID uuid.UUID
	QuestionType QuestionType
	QuestionsData []QuestionData
}

type QuestionData struct {
	Text string
	Value string
}

var questionLogger = log.New(os.Stdout, "Question > ", log.LstdFlags)

func (qt QuestionType) Parse(val string) QuestionType {
	switch val {
		case "Select":
			return SELECT
		case "Text":
			return TEXT
		case "Number":
			return NUMBER
		case "Date":
			return DATE
		case "Boolean":
			return BOOLEAN
		case "Address":
			return ADDRESS
		case "SecureText":
			return SECURE_TEXT
		case "SecureNumber":
			return SECURE_NUMBER
		default:
			return 0 // handle zero??
	}
}

func (qt QuestionType) String() string {
	index := int(qt - 1)
	return questionTypeStrings[index]
}

func (q *Question) Create() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("closing DB")

	if dbc.DB.NewRecord(q) {
		dbc.DB.Create(&q)
		if !dbc.DB.NewRecord(q) {
			questionLogger.Println("Created question: ", q) //todo remove
			return nil
		}
	}

	questionLogger.Println("Failed to create question")  //todo remove

	return errors.New("failed question creation")
}

func (q *Question) Set() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	question := Question{}

	dbc.DB.First(&question, "id = ?", q.ID).Update(q)

	if utils.CompareModelValues(question, q) {
		q = &question
		return nil
	}

	return errors.New("failed to update question")
}

func (q Question) GetByGroupID(groupID uuid.UUID) ([]*Question, error) {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	var questions []*Question

	dbc.DB.Model(q).Find(&questions, "question_group_id = ?", groupID)
	if len(questions) > 0 {
		return questions, nil
	}

	return questions, errors.New(fmt.Sprintf("no questions found for question group %x", groupID))
}

func (q *Question) GetByID(questionID uuid.UUID) error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	question := Question{}

	dbc.DB.Model(q).First(&question, "question_id = ?", q.QuestionID)
	q = &question

	if question.QuestionID == questionID {
		return nil
	}

	return errors.New(fmt.Sprintf("No questions found for question group %x", questionID))
}

func (q Question) GetAll() ([]*Question, error) {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer questionLogger.Println("Closing DB")

	var questions []*Question

	dbc.DB.Find(&questions)
	if len(questions) > 0 {
		return questions, nil
	}

	return questions, errors.New("no questions found")
}