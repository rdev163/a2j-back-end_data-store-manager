package models

type Request struct {
	AuthToken string
	Requester string
	Type string
	ForeignKey string
}
