package models

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/clients"
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/utils"

	"github.com/lib/pq"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type UserRole int

const (
	Undefined UserRole = iota
	Applicant
	Clerk
	Admin
)

type EndUser struct {
	gorm.Model
	UUID uuid.UUID
	Username string
	PrimaryEmail string
	Emails pq.StringArray `gorm:"type:string[]"`
	HashedPass []byte
	Role UserRole `gorm:"type:int"`
}

var endUserLogger = log.New(os.Stdout, "EndUser > ", log.LstdFlags)

func (eu *EndUser) Fill(m map[string]interface{}) error {

	for k, v := range m {
		err := utils.SetField(eu, k, v)
		if err != nil {
			return err
		}
	}

	return nil
}

func (eu *EndUser) Create() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("closing DB")

	modelsLogger.Println("Create", eu) //todo Remove

	if dbc.DB.NewRecord(eu) {
		dbc.DB.Create(&eu)
		if !dbc.DB.NewRecord(eu) {

			endUserLogger.Println("created EndUser: ", eu) //todo Remove

			return nil
		}
	}

	endUserLogger.Println("failed to create EndUser")

	return errors.New("failed to create EndUser")
}

func (eu *EndUser) Update() error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("Closing DB")

	endUser := EndUser{}

	dbc.DB.First(endUser, "id = ?", eu.ID).Update(eu)
	if utils.CompareModelValues(endUser, eu) {
		return nil
	}

	return errors.New("failed to update endUser")
}

func (eu *EndUser) GetByID(endUserID uuid.UUID) (*EndUser, error) {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("Closing DB")

	endUser := EndUser{}

	dbc.DB.Model(eu).First(&endUser, "uuid = ?", endUserID)
	if endUser.UUID == endUserID {
		return &endUser, nil
	}

	return &endUser, errors.New(fmt.Sprintf("No endUsers found for endUser group %x", endUserID))
}

func (eu *EndUser) GetByEmail(email string) error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("Closing DB")

	endUser := EndUser{}

	dbc.DB.Model(eu).First(&endUser, "primary_email = ?", email)

	if dbc.DB.NewRecord(endUser) {
		return errors.New(fmt.Sprintf("No EndUsers found for email %x", email))
	}

	eu = &endUser

	return nil
}

func (eu *EndUser) GetByUsername(username string) error {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("Closing DB")

	endUser := EndUser{}

	dbc.DB.Model(eu).First(&endUser, "username = ?", username)

	if dbc.DB.NewRecord(endUser) {
		return errors.New(fmt.Sprintf("No EndUsers found for username %x", username))
	}

	eu = &endUser

	return nil
}

func (eu *EndUser) VerifyEmail(email string) bool {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("Closing DB")

	endUsers := []EndUser{}

	dbc.DB.Model(&EndUser{}).Find(&endUsers, "primary_email = ?", email)
	if len(endUsers) > 0 {
		return true
	}

	return false
}


func (eu *EndUser) VerifyUsername(un string) bool {

	dbc := clients.DefaultDBConnection()
	defer dbc.DB.Close()
	defer endUserLogger.Println("Closing DB")

	endUsers := []EndUser{}

	dbc.DB.Model(&EndUser{}).Find(&endUsers, "username = ?", un)
	if len(endUsers) > 0 {
		return true
	}

	return false
}