package models

import (
	"errors"
	"log"
	"os"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/clients"
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/utils"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Answer struct {
	gorm.Model
	Profile Profile
	ProfileID uuid.UUID
	Question Question
	QuestionID uuid.UUID
	Payload string
}

var answersLogger = log.New(os.Stdout, "Answers > ", log.LstdFlags)

func (a *Answer) Create() (*Answer, error) {
	dbc := clients.DefaultDBConnection()
	if dbc.Error != nil {
		answersLogger.Panicln("DBConnection Failed")
	}
	defer dbc.DB.Close()
	defer answersLogger.Println("Closing DB")

	if dbc.DB.NewRecord(a) {

		dbc.DB.Create(a)

		if dbc.DB.NewRecord(a) {
			return a, nil
		}
 	}

	return a, errors.New("failed creating answer")
}

/**
Should have a PID already if updating
 */
func (a *Answer) Update(updatedAnswer *Answer) (*Answer, error) {
	dbc := clients.DefaultDBConnection()
	if dbc.Error != nil {
		answersLogger.Panicln("DBConnection Failed")
	}
	defer dbc.DB.Close()
	defer answersLogger.Println("Closing DB")


	dbc.DB.Find(a, "ProfileID = ?", updatedAnswer.ProfileID).Update(updatedAnswer)
	if utils.CompareModelValues(a, updatedAnswer) {
		return a, nil
	}

	return a, errors.New("failed to update an answer")
}

func (a *Answer) GetWithPID(pid uuid.UUID) (*[]Answer, error) {
	dbc := clients.DefaultDBConnection()
	if dbc.Error != nil {
		answersLogger.Panicln("DBConnection Failed")
	}
	defer dbc.DB.Close()
	defer answersLogger.Println("Closing DB")


	var models []Answer

	dbc.DB.First(&models, "ProfileID = ?", pid)

	if len(models) > 0 {

		return &models, nil

	}

	return &models, errors.New("answer not found")
}
