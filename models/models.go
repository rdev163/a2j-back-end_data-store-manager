package models

import (
	"errors"
	"log"
	"os"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/clients"
)

// Making enums until I can make it discover and build it's own models
type A2JModel int

type ModelSpec map[string] interface{}

type FillA2M func(map[string]interface{}) (interface{}, error)

const (
	ENDUSER A2JModel = iota + 1
	PROFILE
	QUESTION
	ANSWER
)

func (a2m A2JModel) String() string {
	strings := [...]string{
		"EndUser",
		"Profile",
		"Question",
		"Answer"}
	return strings[a2m]
}

var modelsLogger = log.New(os.Stdout, "Models > ", log.LstdFlags)

func (a2m A2JModel) New(spec ModelSpec) (error) {

	modelsLogger.Println("hit")
	switch a2m {
		case 1:
			model := EndUser{}
			err := model.Fill(spec)
			if err != nil {
				modelsLogger.Println("fill failed")
			}
			dbc := clients.DefaultDBConnection()
			if dbc.Error != nil {
				modelsLogger.Panicln("DBConnection failed")
				return errors.New("DBConnection failed")
			}
			defer dbc.DB.Close()
			defer modelsLogger.Println("Closing DB")

			if dbc.DB.NewRecord(model) {
				dbc.DB.Create(&model)
				if !dbc.DB.NewRecord(model) {
					modelsLogger.Println("Created: ", model)
					return nil
				}
				modelsLogger.Panicln("Failed to create!: ", model)
				return errors.New("failed to make new model")
			}

		default:

		modelsLogger.Println("hit")

	}
	modelsLogger.Panicln("failed to make instantiate model")
	return errors.New("failed to instantiate model")
}

/**
Runs Re/Initialization for GORM and SQL Tables.
 */
func Initialize() {
	dbc := clients.DefaultDBConnection()
	if dbc.Error != nil {
		modelsLogger.Panicln("DBConnection Failed")
	}
	defer dbc.DB.Close()
	defer modelsLogger.Println("Closing DB")


	dbc.DB.AutoMigrate(&EndUser{}, &Question{}, &QuestionsGroup{}, &Answer{}) //automatically creates "new" tables and cols.
	// Set Relations
	//var answers []Answer

	//dbc.DB.Model(&Profile{}).Related(&answers)

}