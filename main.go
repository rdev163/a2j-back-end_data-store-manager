package main

import (
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/models"
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/procedures"
	"log"
	"os"
	"os/signal"
)

func main() {

	logger := log.New(os.Stdout, "Client Logger > ", log.LstdFlags)

	logger.Println("hit!")

	models.Initialize()

	procedures.InitializeProcedures()

	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, os.Interrupt)

	//err := models.ENDUSER.New(map[string]interface{}{"Username": "FooBarBaz"})
	//if err != nil {
	//	logger.Panicln("New Model Error: ", err)
	//}
	//logger.Println("Returned Model??, working on it")

	select {

	case <-sigChan:

		logger.Println("Shutting Down!")
	}
}
