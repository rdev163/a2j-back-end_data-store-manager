# a2j-back-end_data-store-manager

[![Waffle.io - Columns and their card count](https://badge.waffle.io/CodeForPortland/a2j-back-end_data-store-manager.svg?columns=all)](https://waffle.io/CodeForPortland/a2j-back-end_data-store-manager)

The back end client for providing procedural CRUD tooling.
