package configs

import (
	"github.com/spf13/viper"
	"log"
	"os"
)
var configsLogger = log.New(os.Stdout, "Configs > ", log.LstdFlags)

type Realm struct {
	Url string
	Realm string
}

type Realms struct {
	Internal Realm
}

type Configs struct {
	Realms Realms
}

var defaultConfigs = Configs{Realms: Realms{Internal: Realm{Url: "localhost:3131", Realm: "aura.ctl.internal"}}}

func (c *Configs) Init() {
	cwd, err := os.Getwd()
	viper.SetConfigName("client.config")
	viper.SetConfigType("json")
	viper.AddConfigPath(cwd + "/local")
	err = viper.ReadInConfig()
	if err != nil {
		configsLogger.Println("local/client.config.json not found, falling back to default")
		c = &defaultConfigs
	}
	// internal
	url, ok := viper.Get("realms.internal.url").(string)
		if !ok {
			configsLogger.Println("Not OK --> did not receive URL")
		}
	realm, ok := viper.Get("realms.internal.realm").(string)
		if !ok {
			configsLogger.Println("Not OK --> did not receive realm")
		}
	c.Realms.Internal = Realm{url, realm}
}