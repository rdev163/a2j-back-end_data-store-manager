package handlers

import (
	mocket "github.com/selvatico/go-mocket"
	"github.com/jinzhu/gorm"
)

func SetupTests() (*gorm.DB, error) { // or *gorm.DB
  mocket.Catcher.Register() // Safe register. Allowed multiple calls to save
  mocket.Catcher.Logging = true
  // GORM
  return gorm.Open(mocket.DriverName, "connection_string") // Can be any connection string

}

