package handlers

import (
	"context"
	"github.com/kataras/iris/core/errors"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/models"

	"github.com/gammazero/nexus/client"
	"github.com/gammazero/nexus/wamp"
)

/**
 * Retrieves end user data with an email or username depending on message type.
 */
var ReadEndUser client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	if err != nil {
		handlersLogger.Println("failed to handle procedure message: ", err)
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}

	switch message.MessageType {

		case "Email":

			endUser := &models.EndUser{}
			email, ok := message.Payload["email"].(string)
			if !ok {
				handlersLogger.Println("no valid email value found: ", err)
				return &client.InvokeResult{Err: wamp.ErrProtocolViolation}
			}

			err := endUser.GetByEmail(email)
			if err != nil {
				handlersLogger.Println("failed to find end user by email: ", email)
				return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Payload": wamp.Dict{ "found": false, "user": models.EndUser{}}}}
			}

			return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Payload": wamp.Dict{ "found": true, "user": endUser}}}

		case "Username":

			endUser := &models.EndUser{}
			username, ok := message.Payload["username"].(string)
			if !ok {
				handlersLogger.Println("no valid username value found: ", err)
				return &client.InvokeResult{Err: wamp.ErrProtocolViolation}
			}

			err := endUser.GetByUsername(username)
			if err != nil {
				handlersLogger.Println("failed to find end user by username: ", username)
				return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Payload": wamp.Dict{ "found": false, "user": models.EndUser{}}}}
			}

			return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Payload": wamp.Dict{ "found": true, "user": endUser}}}
	}

	return &client.InvokeResult{Err: wamp.ErrCanceled}
}

/**
 * Takes end user data from a procedure call and creates a new end user record.
 */
var CreateEndUser client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	newEndUser, err := endUserDataPayload(message.Payload)
	if err != nil {
		handlersLogger.Println("failed to build EndUser", err)
		return &client.InvokeResult{Err: wamp.ErrCanceled}
	}
	handlersLogger.Println("Creating User", newEndUser)

	err = newEndUser.Create()
	if err != nil {
		return &client.InvokeResult{
			Err: wamp.ErrCanceled,
		}
	}

	return &client.InvokeResult{
		Kwargs: wamp.Dict{"Payload": newEndUser},
	}
}

/**
 * Takes end user data from a procedure call and updates an existing end user record.
 */
var UpdateEndUser client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	updatedEndUser, err := endUserDataPayload(message.Payload)
	if err != nil {
		handlersLogger.Println("failed to build EndUser", err)
		return &client.InvokeResult{Err: wamp.ErrProtocolViolation}
	}

	err = updatedEndUser.Update()
	if err != nil {
		return &client.InvokeResult{
			Kwargs: wamp.Dict{"eventType": "Response", "payload": nil},
			Err: wamp.ErrCanceled,
		}
	}

	return &client.InvokeResult{
		Kwargs: wamp.Dict{"Type": "Response", "Payload": updatedEndUser},
	}
}

/**
 * Checks if an email is already stored
 */
var IfEmail client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	user := &models.EndUser{}
	payload := dicts["Payload"].(string)

	return &client.InvokeResult{Kwargs: wamp.Dict{"Payload": user.VerifyEmail(payload)}}
}

/**
 * Checks if a username is already stored.
 */
var IfUsername client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	user := &models.EndUser{}
	payload := dicts["Payload"].(string)

	return &client.InvokeResult{Kwargs: wamp.Dict{"Payload": user.VerifyUsername(payload)}}
}

/**
 * Builds an EndUser struct from a procedure payload.
 */
func endUserDataPayload(payload map[string]interface{}) (models.EndUser, error) {
	var hashedPass []byte

	username, ok := payload["Username"].(string)
	if !ok {
		return models.EndUser{}, errors.New("invalid user data")
	}

	email, ok := payload["Email"].(string)
	if !ok {

	}

	stringHP, ok := payload[""].(string)
	if !ok {
		logger.Println("hashed pass not a string")
		bytesHP, ok := payload["HashedPass"].([]byte)
		if !ok {
			logger.Println("hashed pass is invalid")
			return models.EndUser{}, errors.New("invalid user data")
		}
		hashedPass = bytesHP
	} else {
		hashedPass = []byte(stringHP)
	}

	return  models.EndUser{
		Username: username,
		PrimaryEmail: email,
		HashedPass: hashedPass,
	}, nil
}