package handlers

import (
	"context"
	"errors"

	"github.com/CodeForPortland/a2j-back-end_data-store-manager/models"

	"github.com/google/uuid"

	"github.com/gammazero/nexus/client"
	"github.com/gammazero/nexus/wamp"
)

type questionMessageData struct {
	label string
	questionID uuid.UUID
	questionGroupTags []string
	questionType models.QuestionType
	questionData []map[string]string
}

/**
 * Extracts question data from incoming procedure call message,
 * creates question ID if not provided and errors out if no label.
 */
func handleQuestionDataPayload(payload map[string]interface{}) (questionMessageData, error) {

	var questionId uuid.UUID
	var questionType models.QuestionType

	label, ok := payload["label"].(string)
	if !ok {
		return questionMessageData{}, errors.New("invalid question data, no label")
	}

	// Parse or Create a UUID for incoming question data.
	idString, ok := payload["questionID"].(string)
	if !ok {
		questionId = uuid.New()
	} else {
		uUID, err := uuid.Parse(idString)
		if err != nil {
			return questionMessageData{}, errors.New("question handler failed to create uuid")
		}
		questionId = uUID
	}

	questionGroupTags, ok := payload["questionGroupTags"].([]string)
	if !ok {
		questionGroupTags = []string{}
	}

	questionData, ok := payload["questionData"].([]map[string]string)
	if !ok {
		questionData = []map[string]string{}
	}

	qtString, ok := payload["questionType"].(string)
	if !ok {
		return questionMessageData{}, errors.New("invalid question data, no type")
	}

	return questionMessageData{
		label: label,
		questionID: questionId,
		questionGroupTags: questionGroupTags,
		questionType: questionType.Parse(qtString),
		questionData: questionData,
	}, nil
}

var GetAllQuestions client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	if err != nil {
		logger.Println("getAllQuestions was called with an invalid message: ", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrProtocolViolation}
	}

	questions, err := models.Question{}.GetAll()
	if err != nil {
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response", "Payload": wamp.Dict{"ok": false, "questions": questions }}}
	}
	return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response", "Payload": wamp.Dict{"ok": true, "questions": questions }}}
}

/**
 * Retrieves and sends an array of Question based on the message Type string value.
 */
var GetQuestions client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {
	message, err := handleProcedureMessage(dicts)
	if err != nil {
		logger.Println("create question was called with an invalid message: ", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrProtocolViolation}
	}

	var questions []*models.Question

	switch message.MessageType {

		case "GroupID":

			gidString, ok := message.Payload["groupID"].(string)
			if !ok {
				err = errors.New("message groupID has invalid data type")
			}
			groupID, err := uuid.Parse(gidString)
			if err != nil {
				logger.Println("failed to parse uuid from message groupID value:", err)
				return &client.InvokeResult{Err: wamp.ErrCanceled}
			}
			q := models.Question{}
			questions, err = q.GetByGroupID(groupID)

		case "QuestionID":

			qid, ok := message.Payload["questionID"].(string)
			if !ok {
				err = errors.New("message questionID has invalid data type")
			}
			questionID, err := uuid.Parse(qid)
			if err != nil {
				logger.Println("failed to parse uuid from message questionID value:", err)
				return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrCanceled}
			}
			q := models.Question{QuestionID: questionID}
			err = q.GetByID(questionID)
			questions = append(questions, &q)
	}

	if err != nil {
		logger.Println("error with GetQuestion", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response", "Payload": wamp.Dict{"ok": false, "questions": questions}}}
	}

	return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response", "Payload": wamp.Dict{"ok": true, "questions": questions}}}

}

/**
 * Updates a Question record based on the data provided in a procedure message, requires a questionID.
 */
var UpdateQuestion client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {
	message, err := handleProcedureMessage(dicts)
	data, err := handleQuestionDataPayload(message.Payload)
	if err != nil {
		logger.Println("create question was called with an invalid message: ", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrProtocolViolation}
	}

	var qds []models.QuestionData

	for _, val := range data.questionData {

		text := string(val["Text"])
		value := string(val["Value"])

		qd := models.QuestionData{Text: text, Value: value}
		qds = append(qds, qd)
	}

	updatedQuestion := models.Question{
		Label:         data.label,
		QuestionID:    data.questionID,
		QuestionType:  data.questionType,
		QuestionsData: qds,
	}
	err = updatedQuestion.Set()
	if err != nil {
		logger.Println("failed to create a question")
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrCanceled}
	}

	logger.Println("question created")
	return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response", "Payload": wamp.Dict{"question": updatedQuestion}}}

}

/**
 * Creates a Question record based on the data extracted from a procedure message.
 */
var CreateQuestion client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	data, err := handleQuestionDataPayload(message.Payload)
	if err != nil {
		logger.Println("create question was called with an invalid message: ", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrProtocolViolation}
	}

	var qds []models.QuestionData

	for _, val := range data.questionData {

		text := string(val["Text"])
		value := string(val["Value"])

		qd := models.QuestionData{Text: text, Value: value}
		qds = append(qds, qd)
	}

	newQuestion := models.Question{
		Label:         data.label,
		QuestionID:    data.questionID,
		QuestionType:  data.questionType,
		QuestionsData: qds,
	}
	err = newQuestion.Create()
	if err != nil {
		logger.Println("failed to create a question")
		return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response"}, Err: wamp.ErrCanceled}
	}

	logger.Println("question created")
	return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Type": "Response", "Payload": wamp.Dict{"question": newQuestion}}}

}

