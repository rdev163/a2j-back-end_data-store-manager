package handlers

import (
	"context"
	"github.com/CodeForPortland/a2j-back-end_data-store-manager/models"
	"github.com/gammazero/nexus/client"
	"github.com/gammazero/nexus/wamp"
	"github.com/google/uuid"
	"github.com/kataras/iris/core/errors"
)


func handleQuestionsGroupMessageData(message ProcedureMessage) (models.QuestionsGroup, []error) {

	var questionsGroupID uuid.UUID
	var questions []*models.Question
	var errs []error

	payload := message.Payload

	// if the ID string isn't supplied it should be ok to provide one.
	qgIDString, ok := payload["questionsGroupID"].(string)
	if !ok {
		errs = append(errs, errors.New("no questionsGroupID string value in procedure message"))
		questionsGroupID = uuid.New()
	} else {
		qgid, err := uuid.Parse(qgIDString)
		if err != nil {
			errs = append(errs, err)
			logger.Println("error creating new QuestionsGroupID")
			qgid = uuid.New()
		}
		questionsGroupID = qgid
	}

	questionsGroupLabel, ok := payload["label"].(string)
	if !ok {
		errs = append(errs, errors.New("questionsGroup groupLabel has no string value"))
		questionsGroupLabel = "no label"
	}

	qs, ok := payload["questions"].([]models.Question)
	if !ok {
		errs = append(errs, errors.New("questionsGroup data has no questions"))
	} else {
		for _, q := range qs {
			questions = append(questions, &q)
		}
	}

	if len(errs) > 0 {
		return models.QuestionsGroup{
			Label: questionsGroupLabel,
			QuestionsGroupID: questionsGroupID,
			Questions: questions,
		}, errs
	}

	return models.QuestionsGroup{
		Label: questionsGroupLabel,
		QuestionsGroupID: questionsGroupID,
		Questions: questions,
	}, nil
}


var GetAllQuestionsGroups client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	if err != nil {
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false}}, Err: wamp.ErrProtocolViolation}
	}

	questionsGroups, err := models.QuestionsGroup{}.GetAll()
	if err != nil {
		logger.Println("failed finding QuestionsGroups", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false, "questionsGroups": questionsGroups}}}
	}
	return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": true, "questionsGroups": questionsGroups}}}
}

var GetQuestionsGroupByID client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	var questionsGroupID uuid.UUID

	message, err := handleProcedureMessage(dicts)
	if err != nil {
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false}}, Err: wamp.ErrProtocolViolation}
	}

	qgidString, ok := message.Payload["questionsGroupID"].(string)
	if !ok {
		qgid, ok := message.Payload["questionsGroupID"].(uuid.UUID)
		if !ok {
			logger.Println("invalid message data for GetQuestionsGroupByID invocation")
			return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Payload": wamp.Dict{"ok": false}}, Err: wamp.ErrCanceled}
		}
		questionsGroupID = qgid
	} else {
		id, err := uuid.Parse(qgidString)
		if err != nil {
			logger.Println("failed to parse uuid from questionsGroupID string:", err)
			return &client.InvokeResult{Kwargs: wamp.Dict{"Headers": message.Headers, "Payload": wamp.Dict{"ok": false}}, Err: wamp.ErrCanceled}
		}
		questionsGroupID = id
	}

	questionsGroup := &models.QuestionsGroup{}

	err = questionsGroup.FindByID(questionsGroupID)
	if err != nil {
		logger.Println("failed finding QuestionsGroups", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false, "questionsGroup": questionsGroup}}}
	}

	return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": true, "questionsGroup": questionsGroup}}}

}

var CreateQuestionsGroup client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	if err != nil {
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false}}, Err: wamp.ErrProtocolViolation}
	}
	questionsGroup, errs := handleQuestionsGroupMessageData(message)

	// todo create error handling. Look, an error for loop!! :)
	for _, err := range errs {
		logger.Println("error handling message data: ", err)
	}

	qg := &questionsGroup

	err = qg.Create()
	if err != nil {
		logger.Println("failed to create QuestionsGroup record:", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false, "questionsGroup": qg}}, Err: wamp.ErrProtocolViolation}
	}

	return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": true, "questionsGroup": qg}}, Err: wamp.ErrProtocolViolation}

}

var UpdateQuestionsGroup client.InvocationHandler = func(context context.Context, lists wamp.List, dicts wamp.Dict, dicts2 wamp.Dict) (result *client.InvokeResult) {

	message, err := handleProcedureMessage(dicts)
	if err != nil {
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false}}, Err: wamp.ErrProtocolViolation}
	}
	questionsGroup, errs := handleQuestionsGroupMessageData(message)

	// todo create error handling. Look, an error for loop!! :)
	for _, err := range errs {
		logger.Println("error handling message data: ", err)
	}

	qg := &questionsGroup

	err = qg.Update()
	if err != nil {
		logger.Println("failed to update QuestionsGroup record:", err)
		return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": false, "questionsGroup": qg}}, Err: wamp.ErrProtocolViolation}
	}

	return &client.InvokeResult{Kwargs: wamp.Dict{"Type": "Response", "Headers": message.Headers, "Payload": wamp.Dict{"ok": true, "questionsGroup": qg}}, Err: wamp.ErrProtocolViolation}

}
