package utils

import (
	"errors"
	"fmt"
	"log"
	"os"
	"reflect"
)

var utilsLogger = log.New(os.Stdout, "Utils > ", log.LstdFlags)

func CompareModelValues(a interface{}, b interface{}) bool {
	return reflect.DeepEqual(a, b) // advised against using reflect for performance. https://stackoverflow.com/questions/24534072/how-to-compare-struct-slice-map-are-equal
}

// alt: https://github.com/mitchellh/mapstructure
// https://stackoverflow.com/questions/26744873/converting-map-to-struct
func SetField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)
	utilsLogger.Println(structValue, structFieldValue, obj, name, value)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("no such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		return errors.New("provided value type didn't match obj field type")
	}

	structFieldValue.Set(val)
	return nil
}
