package utils

import "testing"

type TestStruct struct {
	foo string
	bar int
}
func TestCompareModelValues(t *testing.T) {

	t.Run("Comparing the same struct should return true", func(t *testing.T) {

		struct1 := TestStruct{}

		struct2 := TestStruct{}

		if CompareModelValues(struct1, struct2) {
			return
		}

		t.Error("Equal structs returned false.")
	})

	t.Run("Comparing different structs should return false", func(t *testing.T) {

		struct1 := TestStruct{foo: "Baz", bar: 1}

		struct2 := TestStruct{foo: "qux", bar: 1}

		if CompareModelValues(struct1, struct2) {

			t.Error("Different structs returned true")

		}

		return
	})
}
